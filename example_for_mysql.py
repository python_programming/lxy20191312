"""
author： 20191312lxy

filename： example_for_mysql

time: 2020.4.29

"""
import pymysql

# 如果文件不存在，会自动在当前目录创建
db = pymysql.connect(host="localhost", port=8803, user="root", password="123456", database='student')

# 创建一个游标对象
cursor = db.cursor()
# 执行一条SQL语句，创建user表：注意user表已存在
# 1.创建一个表
cursor.execute(
    'create table if not exists student (number int(10) primary key, name varchar(20), score float) ')  # SQL语句
# 2.向表中插入数据
cursor.execute('insert into student (number, name, score) values (20191312, "刘新宇", 100)')
cursor.execute('insert into student (number, name, score) values (20191320, "李泽昊", 101)')
# 3.查询Select语句
cursor.execute('select * from student')
print(cursor.fetchall())
# 4.修改数据库
cursor.execute('update student set score = 100 where number = 20191320')
cursor.execute('select * from student')
print(cursor.fetchall())
# 5.删除
cursor.execute('delete from student where number = 20191320 ')
cursor.execute('select * from student')
print(cursor.fetchall())
#  关闭数据库
db.close()
