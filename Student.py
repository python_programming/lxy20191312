"""
filename: Student

author: 20191312lxy

time: 2020.4.8
"""


class Student:
    """
    学生的信息
    """
    name = " "
    school = "电科院"
    major = " "
    hobby = " "
    _phone_number = " "

    def __init__(self):
        print("我是学生")


class BoyStudent(Student):
    name = "小明"
    major = "信息安全"
    hobby = "运动、看书"
    _phone_number = "18245678912"

    def __init__(self):
        print("我是男学生")

    def Introduce(self, name, school, major, hobby):
        print("我叫" + name + "来自" + school + "，专业是" + major + "，我喜欢" + hobby)

    def Sport(self, name):
        print(name + "自由的奔跑在操场上")

    def Read(self, name):
        print(name + "在图书馆安静的看书")


class GirlStudent(Student):
    name = "小红"
    major = "保密管理"
    hobby = "看电影、游泳"
    _phone_number = "18241258974"

    def __init__(self):
        print("我是女学生")

    def Introduce(self, name, school, major, hobby):
        print("我叫" + name + "来自" + school + "，专业是" + major + "，我喜欢" + hobby)

    def WatchMovie(self, name):
        print(name + "在电影院安静的看着电影")

    def Swim(self, name):
        print(name + "在游泳池中畅快的游泳")


Ming = BoyStudent()
Ming.Introduce(Ming.name, Ming.school, Ming.major, Ming.hobby)
Ming.Sport(Ming.name)
Ming.Read(Ming.name)
Hong = GirlStudent()
Hong.Introduce(Hong.name, Hong.school, Hong.major, Hong.hobby)
Hong.WatchMovie(Hong.name)
Hong.Swim(Hong.name)
