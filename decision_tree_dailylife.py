'''
author: 20191312lxy

filename:decision_tree

time:2020.3.11
'''

while True:
    n = input("是否开始新的一天")
    if n == 'n':
        break
    n = input("今天是新的一天，是否出门？ y/n:\n")
    if n == 'n':
        print("好的，今天在家")
        n = input("今天要学习吗？ y/n:\n")
        if n == 'n':
            print("好的，今天不学习，我们运动吧！")
        else:
            print("好的，我们今天学习")
            n = input("你想学python吗？ y/n\n")
            if n == 'n':
                print("好的，我们学习C语言！")
            else:
                print("我们来学python吧！")
            continue
    else:
        n = input("带口罩吗？ y/n:\n")
        if n == 'n':
            print("对不起，你在门口被社区大妈拦下，出门失败,你很郁闷的在家待了一天")
            continue
        else:
            n = input("去图书馆吗？ y/n:\n")
            if n == 'n':
                n = input("去电影院吗？ y/n:\n")
                if n == 'n':
                    print("到处逛逛吧。")
                else:
                    print("好的，去看电影吧")
            else:
                n = input("你来到了图书馆，你想看书吗？ y/n: \n")
                if n == 'y':
                    print("你在图书馆看书，一天过去了。你回到家，疲惫的躺在了床上，感觉今天很充实")
                    continue
                else:
                    print("你在图书馆打游戏，一天就过去了。你回到家，感觉很惭愧，并且决定明天一定要好好学习")
