"""
author: 20191312lxy

filename:experiment_1

time: 2020.4.12
"""


"""
一个简单的计算器
"""

import math


while 1:
    choice = input("是否开始计算 y/n\n")
    if choice == 'n':
        break
    num_1 = float(input("输入需要计算的数：\n"))
    choice = input("选择需要进行的运算：\n1.加法\n2.减法\n3.乘法\n4.除法\n5.求余\n6.高级运算\n")
    if choice == '1':
        num_2 = float(input("这个数要加上："))
        print(f"{num_1} + {num_2} = {num_1 + num_2}\n")
        continue
    if choice == '2':
        num_2 = float(input("这个数要减去："))
        print(f"{num_1} + {num_2} = {num_1 - num_2}\n")
        continue
    if choice == '3':
        num_2 = float(input("这个数要乘以："))
        print(f"{num_1} + {num_2} = {num_1 * num_2}\n")
        continue
    if choice == '4':
        num_2 = float(input("这个数要除以："))
        print(f"{num_1} + {num_2} = {num_1 / num_2}\n")
        continue
    if choice == '5':
        num_2 = float(input("这个数要对什么数求余："))
        print(f"{num_1} + {num_2} = {num_1 % num_2}\n")
        continue
    if choice == '6':
        choice = input("需要进行什么运算：\n1.阶乘\n2.乘方\n3.开平方\n4.求sin值\n5.求cos值\n")
        if choice == '1':
            print(f"{num_1}! = {math.factorial(num_1)}\n")
            continue
        if choice == '2':
            num_2 = float(input("求几次方：\n"))
            print(f"{num_1}^{num_2} = {math.pow(num_1,num_2)}\n")
            continue
        if choice == '3':
            print(f"结果为：{math.sqrt(num_1)}\n")
            continue
        if choice == '4':
            print(f"结果为：{math.sin(num_1)}\n")
            continue
        if choice == '5':
            print(f"结果为：{math.cos(num_1)}\n")
            continue






