"""
author: 20191312

filename: list_test

time : 2020.3.18
"""

# 第一题

tanks = ["猪八戒", "嫦娥", "孙策", "梦奇", "苏烈", "项羽", "白起"]

fighters = ["马超", "曜", "云中君", "盘古", "李信", "橘右京", "达摩", "宫本武藏"]

assassins = ["上官婉儿", "司马懿", "荆轲", "元歌", "李白", "韩信", "孙悟空"]

masters = ["小乔", "貂蝉", "高渐离", "嬴政", "妲己", "弈星", "武则天"]

shooters = ["孙尚香", "虞姬", "百里守约", "黄忠", "李元芳", "后羿", "艾琳"]

assistants = ["盾山", "姜子牙", "太乙真人", "孙膑", "刘禅", "庄周", "大乔"]

print("*****坦克*****")
for tank in tanks:
    print(tank, end="、")
print()
print("*****战士*****")
for fighter in fighters:
    print(fighter, end="、")
print()
print("*****刺客*****")
for assassin in assassins:
    print(assassin, end="、")
print()
print("*****法师*****")
for master in masters:
    print(master, end="、")
print()
print("*****射手*****")
for shooter in shooters:
    print(shooter, end="、")
print()
print("*****辅助*****")
for assistant in assistants:
    print(assistant, end="、")
print()
print("************新版本************")

print("*****新辅助：鲁班大师*****")
assistants.append("鲁班大师")
print(assistants)

print("*****删除射手：艾琳*****")
del shooters[6]
print(shooters)

print("*****修改英雄：荆轲>>>阿珂*****")
assassins[2] = "阿珂"
print(assassins)

# 第二题
# 数据来源：https://www.bilibili.com/ranking?spm_id_from=333.851.b_7072696d61727950616765546162.3
# 时间： 2020.3.18 19:55
# 播放量单位：万次

ViewCounts = [
    ("每日一遍，智商再见", 418.9),
    ("你踩疼我了！", 146.0),
    ("我的老婆们突然叫爸爸（配音模仿）", 174.3),
    ("【自制】技术宅UP耗时三个月，自制B站最强小电视！【极度硬核】【3分钟从草图到实物】", 144.5),
    ("【冰雪奇缘】步 入 迷 惑", 122.3),
    ("【师生对线】这网课上得就尼玛离谱！（第四集）", 176.7),
    ("【新番导视】9.9分预定！我的青春回来了！", 273.5),
    ("2020逆风翻盘！疫情结束后的新机会", 128.8),
    ("他还认识我吗？", 147.8),
    ("“ᴰᵒ ʸᵒᵘ ᴸⁱᵏᵉ ᴿᵃⁱⁿ” “ᴵ ᴾʳᵉᶠᵉʳ ʸᵒᵘ”", 271.3)
]

ViewCounts = sorted(ViewCounts, key=lambda s: s[1], reverse=True)

print("******B站20020年3月18日榜单******")
for index, ViewCount in enumerate(ViewCounts):
    print(index+1, ViewCount)

print("******榜单更新******")

del ViewCounts[2]
ViewCounts[0] = ("【老番茄】史上最骚杀手(第八集)", 600.3)
ViewCounts.append(("【罗翔】买了假烟送领导，卖烟的人倒贴了60元？", 300.7))
ViewCounts = sorted(ViewCounts, key=lambda s: s[1], reverse=True)


for index, ViewCount in enumerate(ViewCounts):
    print(index+1, ViewCount)