"""
author: 20191312lxy

filename:SocketTCPServer

time:2020.5.6
"""

# TCP客户端
# 1.创建套接字，连接远程地址
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.connect(('127.0.01', 8001))
# 2.连接后发送数据和接受数据
# s.sendall(message.encode())
# data = s.recv(1024)
# 3.传输完毕后，关闭套接字
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8001))
message = input("请输入要传输的内容：")
s.sendall(message.encode())
data = s.recv(1024)
print(data.decode())
s.close()

