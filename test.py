import xlwt
import random
import RandomName

wbk = xlwt.Workbook()
sheet = wbk.add_sheet('0321')
headings = ["序号", "姓名", "性别", "学号", "班级", "成绩", "健康状况"]
for item in range(0, len(headings)):
    sheet.write(0, item, headings[item])
for i in range(1, 100):
    for item in range(0, len(headings)):
        number = str(random.randint(20191100, 20199999))
        status = random.choice(["健康", "不健康"])
        name = RandomName.random_name()
        content = [str(i), name.split("\t")[0], name.split("\t")[1], number,
                   number[0:6], str(random.randint(0, 100)), status]
        sheet.write(i, item, content[item])

wbk.save("info_2.xls")