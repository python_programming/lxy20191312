"""
author: 20191312lxy

filename:SocketTCPClient

time:2020.5.6
"""

import socket

# TCP服务器端
# 1.创建套接字， 绑定端口
# 2.开始监听
# 3.进入循环，不断接受客户端连接请求
# 4.然后接受传来的数据，并发送给对方数据
# 5.传输完毕后，关闭套接字
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8001))
s.listen()
conn, address = s.accept()
data = conn.recv(1024)
print(data.decode())
conn.sendall(("服务器已经接受到了数据内容："+str(data.decode())).encode())
s.close()
