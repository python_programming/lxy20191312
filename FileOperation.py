"""
author： 20191312lxy

filename： FileOperation

time: 2020.4.15

"""

import os

# 文件打开
# file = open("test", "r")
# print(file.readline())
# list = file.readlines()

# for item in list:
#    print(item)

#  2.文件的创建
# 以只写模式打开文件不可读,改为w+可读但是没有输出,这是因为输入内容后光标在末尾

# path = r"C:\Users\FERRY\Desktop\test2"
#
# file = open(path, "w+")
# # file = open("test2", "w+")
# file.write("hello world\n")
# file.write("\n")
# file.write("hello ferry")
# print(file.readlines())
# file.close()
# file = open(path, "r")
# file = open("test2", "w+")
# print(file.readlines())
# lst = file.readlines()
# for item in lst:
#     print(item)

# 删除文件

# file.close()
# os.remove("test2")

# #  创建文件夹
#
# print(os.getcwd())
# # print(os.mkdir(r"C:\Users\FERRY\Desktop\None"))
#
# if os.path.exists(r"C:\Users\FERRY\Desktop\None"):
#     tuple1 = os.walk(r"C:\Users\FERRY\Desktop\None")
#     for i in tuple1:
#         print(i)
# else:
#     print("该目录不存在")

# 基本内容

#
path = r"C:\Users\FERRY\Desktop"
os.mkdir(r"C:\Users\FERRY\Desktop\1913")
os.chdir(r"C:\Users\FERRY\Desktop\1913")
file = open("191312.txt", "a")
file.write("20191312 刘新宇 Python程序设计 100 \n")
file.write("20191322 吴泳琳 Python程序设计 100 \n")
file.close()
file = open("191312.txt", "r")
information = file.readlines()
for item in information:
    print(item)
file.close()

# 加分内容
import docx

Doc = docx.Document()
Doc.add_heading("使用Python做Doc")
Doc.add_paragraph("用python做doc很方便")
Doc.add_paragraph("用python做doc很快")
Doc.add_paragraph("用python做doc很有意思")
Doc.save("test.doc")

import xlwt
import random
import RandomName

wbk = xlwt.Workbook()
sheet = wbk.add_sheet('0321')
headings = ["序号", "姓名", "性别", "学号", "班级", "成绩", "健康状况"]
for item in range(0, len(headings)):
    sheet.write(0, item, headings[item])
for i in range(1, 100):
    for item in range(0, len(headings)):
        number = str(random.randint(20191100, 20199999))
        status = random.choice(["健康", "不健康"])
        name = RandomName.random_name()
        content = [str(i), name.split("\t")[0], name.split("\t")[1], number,
                   number[0:6], str(random.randint(0, 100)), status]
        sheet.write(i, item, content[item])

wbk.save("info_2.xls")
