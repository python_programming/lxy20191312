"""
author:20191312lxy

filename: TrainInformation

time:2020.3.26
"""

import re

title = ("车次", "出发站-到达站", "出发时间", "到达时间", "历时")
information = [("T40", "长春-北京", "00:12", "12:20", "12:08"),
               ("T298", "长春-北京", "00:06", "10:50", "10:44"),
               ("Z158", "长春-北京", "12:48", "21:06", "08:18"),
               ("Z62", "长春-北京", "21:58", "06:08", "8:20")]

while 1:
    print("当前的火车信息：")
    print("{:<8}{:<8}{:<8}{:<8}{:<8}".format(*title))
    for i in information:
        print("{:<10}{:<10}{:<10}{:<10}{:<10}".format(*i))
    s = input("是否修改火车信息？y/n\n")
    if s == 'y' or s == 'Y':
        i1 = input("输入车次：")
        i2 = input("输入出发站-到达站：")
        i3 = input("输入出发时间：")
        i4 = input("输入到达时间：")
        i5 = input("输入历时：")
        new = [i1, i2, i3, i4, i5]
        tuple(new)
        information.append(new)
    else:
        break
