"""
filename:Constellation

author:20191312lxy

time: 2020.4.1
"""
"""
水瓶座：1月21日 - 2月19日

双鱼座：2月20日 - 3月20日

白羊座：3月21日 - 4月20日

金牛座：4月21日 - 5月21日

双子座：5月22日 - 6月21日

巨蟹座：6月22日 - 7月22日

狮子座：7月23日 - 8月23日

处女座：8月24日 - 9月23日

天秤座：9月24日 - 10月23日

天蝎座：10月24日 - 11月22日 

射手座：11月23日 - 12月21日

摩羯座：12月22日 - 1月20日
"""


def Constellation(month, day):
    """

    :param month: 出生月份
    :param day: 出生日期
    :return: NONE

    """
    if month == 1:
        if day < 21:
            print("摩羯座")
        else:
            print("水瓶座")
    elif month == 2:
        if day < 20:
            print("水瓶座")
        else:
            print("双鱼座")
    elif month == 3:
        if day < 21:
            print("双鱼座")
        else:
            print("白羊座")
    elif month == 4:
        if day < 21:
            print("白羊座")
        else:
            print("金牛座")
    elif month == 5:
        if day < 22:
            print("金牛座")
        else:
            print("双子座")
    elif month == 6:
        if day < 22:
            print("双子座")
        else:
            print("巨蟹座")
    elif month == 7:
        if day < 23:
            print("巨蟹座")
        else:
            print("狮子座")
    elif month == 8:
        if day < 24:
            print("狮子座")
        else:
            print("处女座")
    elif month == 9:
        if day < 24:
            print("处女座")
        else:
            print("天秤座")
    elif month == 10:
        if day < 24:
            print("天秤座")
        else:
            print("天蝎座")
    elif month == 11:
        if day < 23:
            print("天蝎座")
        else:
            print("射手座")
    elif month == 12:
        if day < 22:
            print("射手座")
        else:
            print("摩羯座")


while 1:
    n = input("是否测算星座：y/n\n")
    if n == 'n':
        print("**************已退出**************")
        break
    user_month = int(input("请输入你的出生月份（例如：2）："))
    user_day = int(input("请输入你的出生日期（例如：21）："))
    Constellation(user_month, user_day)
